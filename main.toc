\contentsline {chapter}{\numberline {1}Introduction to Symphony Framework}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Why bother?}{1}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}System Requirements}{1}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Limitations}{1}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}User Requirements}{1}{subsection.1.1.4}
\contentsline {chapter}{\numberline {2}Symphony: A Framework for Accurate and Holistic WSN Simulation}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Previous Work}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Symphony - System Architecture}{7}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Models of Operating Scopes and Profiling Principles}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Details of Symphony Integration and Usage}{10}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Software Scope}{11}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Application Tier}{11}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Operating System Tier}{13}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Driver Tier}{13}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Hardware Scope}{14}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}The Clock Model - Simulating Time Skew}{15}{subsection.2.5.1}
\contentsline {section}{\numberline {2.6}Data Feed Scope}{17}{section.2.6}
\contentsline {section}{\numberline {2.7}An Experimental Showcase and Performance Metrics for Symphony}{19}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Performance of the showcase scenario}{20}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Symphony's run time performance }{21}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}Conclusions and Future Work}{22}{section.2.8}
\contentsline {chapter}{\numberline {3}Starting programming with Symphony}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Framework Installation}{25}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Preparing your self}{25}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Preparing Your System}{26}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Installing Needed Components}{26}{subsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.3.1}Installing NS3}{26}{subsubsection.3.1.3.1}
\contentsline {subsubsection}{\numberline {3.1.3.2}Installing TinyOS}{28}{subsubsection.3.1.3.2}
\contentsline {subsubsection}{\numberline {3.1.3.3}Elf-loader}{28}{subsubsection.3.1.3.3}
\contentsline {subsubsection}{\numberline {3.1.3.4}XML parser for loading the node model}{29}{subsubsection.3.1.3.4}
\contentsline {subsection}{\numberline {3.1.4}Setting Up Framework}{29}{subsection.3.1.4}
\contentsline {paragraph}{Install Eclipse}{29}{section*.5}
\contentsline {subsection}{\numberline {3.1.5}Running Examples}{30}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Further Reading}{30}{section.3.2}
\contentsline {chapter}{\numberline {4}Programming Symphony Framework}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}The Core of Symphony}{31}{section.4.1}
\contentsline {section}{\numberline {4.2}Integration of Symphony for TinyOS}{31}{section.4.2}
\contentsline {section}{\numberline {4.3}Examples}{31}{section.4.3}
\contentsline {chapter}{\numberline {5}Extending Symphony Framework}{33}{chapter.5}
\contentsline {section}{\numberline {5.1}Extending Symphony}{33}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Adding new TinyOS platform}{33}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Adding new models}{33}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Adding new OS}{33}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Using glib-c}{33}{section.5.2}
\contentsline {chapter}{\numberline {6}Final Notes}{35}{chapter.6}
\contentsline {section}{\numberline {6.1}Future Work and List of Improvements}{35}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}List of Improvements}{35}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}ToDo\'s}{35}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Future Work}{35}{subsection.6.1.3}
\contentsline {chapter}{Index}{37}{chapter*.6}
