\section{Symphony - System Architecture}
\label{sec:symphony}

Figure \ref{fig:symphony_system_arch} illustrates the core architecture of Symphony and its four programming scopes. The \textit{software scope} deals with the mapping of function calls to the underlying hardware scope. The level of abstraction is configurable, and the scheduler of the underlying WSN OS is preserved. The \textit{hardware scope} consists of a clock and a series of models for hardware components such as radio devices and sensors. These hardware models ensure that the application code is executed on a device-specific time scale. The 
\textit{data feed scope} contains mechanisms for either actively or passively feeding data to the relevant software handlers or specific sensor nodes. The 
\textit{orchestration scope} is implemented on top of the general purpose network simulator ns-3. It is responsible for integrating all of the other scopes with the sophisticated communication models and ns-3 event scheduling engine to create a holistic simulation environment. All of Symphony’s operational scopes are parameterized using a single XML configuration file.

\begin{figure}[!th]
 \centering
\includegraphics[width=0.8\textwidth]{system-design-v2.pdf}
 \caption{Architecture of the Symphony framework.}
 \label{fig:symphony_system_arch}
\end{figure}

% \begin{figure}[!ht]
% \centering 
% \includegraphics[width=1\textwidth]{hw_model_abstract.png} 
%  \caption{Example of model relation with hardware emulation and different 
% levels of abstractions.} 
% \label{fig:os-call-hw-example_xml}
%  \end{figure}

\subsection{Models of Operating Scopes and Profiling Principles}

In Symphony, nodes are simulated using a set of models that provide a homogeneous description of the behaviors of both hardware and software components in terms of execution times and energy consumption.  \autoref{fig:os-call-example} provides a graphical illustration of this approach to modeling, while \autoref{lst:configuration_example} shows the XML configuration of the model. The figure shows three components,  \textit{C1, C2,} and \textit{C3}, which describe functionality at different levels of granularity. \textit{C1} is the lowest level, i.e. hardware, component and may represent something like a radio device and the associated driver. It performs the primitive operations of sending and receiving bytes. The \textit{C2} component represents a higher level of functionality, such as a function that queues packets, inspects and modifies their headers, and then transmits them onwards. Finally,  \textit{C3} represents the highest level software components, such as functions that accept packets, encrypt and 
decrypt them, and perform application-specific operations before transmitting them onwards. The level of granularity in the simulation can be configured by the user. For example, it is possible to perform system-level experiments using only application-level components, or, on the other extreme, to focus on low-level operations using driver-level models. Simulations of the latter sort are particularly useful for very fine-grained debugging.

 \begin{figure}[!ht]
\centering 
\includegraphics[width=0.8\textwidth]{hw_model_example.pdf} 
 \caption{Model configuration using xml and hardware models.} 
\label{fig:os-call-example}
 \end{figure}
 
The component models describe the component’s time and energy properties when it is called  (\texttt{call}) and when it returns control to its caller (\texttt{callbacks}). The specific time and energy values for each element are defined in its attributes, as shown in  \autoref{lst:configuration_example}.



\begin{minipage}{\columnwidth}
\lstset{language=XML,
caption={Part of a device model in XML format describing the execution time and energy consumption of a representative component. },
xleftmargin=3mm,
label=lst:configuration_example,
,escapeinside={@}{@}
}
\begin{lstlisting}
<symphony>
    <scope name=hardware>
    <model name="C1" time_unit="micro" energy_unit="mA">
      <call name="START_1"  />
      <callback return="int" params="1" param1="void *" time="60" energy="3" name="DONE_1"/>
      ...
    </model>
  <scope name=software>
    <model name="C2" time_unit="micro" energy_unit="mA">
      <call name="START_2"/>
      <callback return="int" params="2" param1="uint8_t" param2="void *" time="550" energy="10" name="DONE_2"/>
      ...
    </model>
  <scope name=software>
    <model name="C3" time_unit="micro" energy_unit="mA">
      <call name="START_3" time="35" energy="1" />
      <callback return="int" params="1" param1="uint8_t"  time="350" energy="8" name="DONE_3"/>
      ...
    </model>
  </scope>
  ...
</symphony>
\end{lstlisting}
\end{minipage}
\vspace{2mm}

The component models also describe the properties of \textit{callback}. These include information on the return type and the input parameters of the function. In the example shown in \autoref{lst:configuration_example}, the time and energy values were determined by measuring the time required to complete a specific operation and the energy consumed when doing so for a specific device. The acquisition of such measurements is referred to as  \textit{profiling}.
 
Profiling is typically performed as part of a systematic measurement campaign. The best way of determining the execution time and the energy consumption of a specific component is to use external measuring equipment. We anticipate that a library of profiles for different components and platforms will be assembled over time and made available to Symphony users. The profiles discussed in this article were generated using a high accuracy 24-bit NI-4461-ADC analog to digital converter PCI card manufactured by National Instruments. The AD-card was connected to a board that was used to supply power to the node. The experiments were performed on a platform featuring a 16-bit micro controller with a maximum speed of 20 MHz, 31kB of RAM, an IEEE 802.15.4 compatible radio transceiver, several on-board sensors, and the A/D converter \cite{ril_tinymulle}. A range of components based on the TinyOS operating system have been profiled already to showcase the process, including a raw radio interface (representing the lowest 
level of abstraction), the ActiveMessage component (representing an intermediate level of abstraction), and several different security functionalities (representing the highest levels of abstraction).

While all of the showcases presented in this article use the TinyOS operating system, Symphony offers a generic virtualization platform. The next subsection discusses Symphony’s assembly and usage patterns that are common to all WSN operating systems.




\subsection{Details of Symphony Integration and Usage}
This section discusses the integration of Symphony’s software, hardware and data feed scopes into a cohesive whole to form a powerful and general simulation environment. The individual scopes are discussed in detail in the following sections. 

Essentially, the Symphony framework allows the user to seamlessly compile their software and then run it either on a real node or inside the simulation environment. In both cases, the execution model of the underlying operating system is preserved. When the software is compiled for Symphony, a binary image of a node’s software is created. This image is then linked to the relevant models when the simulation is initiated. During the simulation’s startup process, the binary file is loaded into the memory and function symbols for matching functions, which are specified in the XML configuration file, are linked to the corresponding model functions using the  \textit{dynamic linking} facilities of C++. This completes the virtualization process, enabling the node to be started inside ns-3. Within the simulated environment, the node runs according to its internal OS scheduler, preserving its original execution model. The emulated ticks of the node’s internal clock are generated using Symphony’s  \textit{clock model}. In this way, Symphony is capable of virtualizing either several \textit{several different instances} of the same operating system or \textit{several different operating systems} and running them within a single simulation


One of the biggest technical challenges when implementing Symphony was the limitations that host operating systems can place on the number of namespaces and static libraries that can be open simultaneously. For example, in Linux this number is currently set to 14. As a partial solution to this problem, a patched version of \textit{elf-loader} \cite{lacage2010} was integrated into Symphony. This makes it possible to load a substantially larger number of node images; in principle, it would be possible to open an unlimited number of static libraries. In practice, the performance of the host hardware will impose an upper boundary on this quantity.
 
Symphony’s simulated scenarios are constructed and executed inside the ns-3 environment. The use of ns-3 as the simulation engine makes it possible to reuse native ns-3 modules and its well-developed communication models. Technically, Symphony also adds a new type of a node model and the associated infrastructure (containers, helpers, etc.), which are inherited from the base classes of ns-3. From the user’s perspective, however, the simulation workflow remains the same as that for the standard version of ns-3, which is shown in Listing
\ref{lst:simulation_example}. Similarly, the simulation’s progress is logged using the native simulation logging functionality of ns-3.

\begin{minipage}{\columnwidth}
\lstset{language=C++,caption={The set-up of simulations in the ns-3 environment.
},xleftmargin=3mm,
xrightmargin=3mm,
label=lst:simulation_example}
\begin{lstlisting}
#include <stdio.h>
...//Standard ns-3 modules are omitted.
#include "ns3/symphony-module.h"
using namespace ns3;
int main(int argc, char *argv[]) {
  ...
  TosNodeContainer c; //enables TinyOS nodes
  c.Create(10,  "libtossecurity.so"); //creates ten nodes with os the image to libtossecurity.so
  TosHelper wifi;
  wifi.SetStandard(ZIGBEE_PHY_STANDARD_802154); //creates wireles standard
  wifi.SetNodeModel("tos-security.xml");
  YansTosPhyHelper wifiPhy = YansTosPhyHelper::Default(); //creates wifi channel
  wifiPhy.Set("RxGain", DoubleValue(0));
  ...
  TosNetDeviceContainer devices = wifi.Install(wifiPhy, c); //installs wifi devices
  TosMobilityHelper mobility;
  ... 
}
\end{lstlisting}
\end{minipage}
\vspace{2mm}



















