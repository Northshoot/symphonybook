\section{Introduction}
\label{sec:intro}
Traditionally in the area of communication networks simulations is the primary
technique for the analysis of performance of various protocols. This is also
true in the case of performance studies of wireless sensor networks. WSNs,
however, have one important peculiarity, which makes simulation-based studies
challenging. Most, if not all, network simulators execute the experiment
scenario in high-end machines. In WSNs the resource constrained hardware
is in many cases the primary performance limiting factor. Ignoring either the
effects of the hardware delays, the time skew, the delays induced by sensory 
data flow or the particular execution model of operating
systems makes the obtained performance figures unrealistic.


In this article we describe a simulation framework, named Symphony, for testing
and validation of WSN applications which closely resembles the processes
happening inside real equipment including hardware and software induced delays,
and dynamic flow of sensory data. A high level architecture of Symphony is depicted in \autoref{fig:symphony_framework}. The overall objective with Symphony is to provide a holistic framework where the development of WSN software and simulation of its functionality is done within the same integrated development environment. In brief, using Symphony a WSN developer always works with real implementation in one of the existing operating systems for example TinyOS, FreeRTOS or Contiki. Symphony provides a virtualization and hardware modeling techniques which on the one hand gives the developer a feeling of working with real nodes. On the other hand this layer smoothly integrates the real implementation with general purpose network simulator allowing an extensive testing of a distributed functionality in a controlled and repeatable manner. Technically, Symphony consists of four operating and programming scopes: a software scope, a hardware scope, a data feed scope and an orchestration and communication scopes.
%\begin{figure}[!tb]
\begin{wrapfigure}{r}{0.55\textwidth}
 \centering
\includegraphics[width=0.55\textwidth]{symphony-framework.png}
 \caption{High level architecture overview of Symphony.}
 \label{fig:symphony_framework}
\end{wrapfigure}
%\end{figure}

 The \textit{software scope} provides necessary tools and a set of rules for building an existing WSN operating system into a virtual image. The \textit{hardware scope} contains a set of models accurately emulating time and energy characteristics of hardware components. The \textit{data feed scope} provides tools and models for making sensory data available to the
virtualized node. Finally, the \textit{orchestration and communication scope} is provided by a popular network simulator \textit{ns-3}\footnote{http://www.nsnam.org/}, which enables a straightforward creation and execution of various simulation scenarios. 


All in all, Symphony bridges the current gap between simulated and real WSN software. We conjecture that using Symphony the time for developing real large scale distributed WSN systems could shortened substantially. The following features make Symphony a unique development environment, distinct from any currently existing.

\begin{enumerate}
 \item Experimentation with the real code base as in the real deployments;

 \item Preservation of the execution model of the underlying operating system;

\item Accounting for the effect of hardware induced delays on the performance of
distributed applications and protocols;

\item Experimenting with a range of clock skews models;

\item Experimentation with several different applications and different WSN operating systems within the same simulation;

\item A customizable level of simulation details, ranging from a fine grain
firmware emulation to a system level experiments;

\item Experimenting with performance phenomena on the entire path of sensory data.


\end{enumerate}

   

The article is organized as follows. In Section \ref{sec:related} we overview
related work in the domain of simulation-based experimentation with WSN. Section
\ref{sec:symphony} describes the architecture of the framework, introduces programming scopes and elaborates on orchestration and communication scope. Section  \ref{sec:software_scope} goes into the details of Software Scope. Hardware Scope is described in Section \ref{sec:hardware_scope}. Data Feed Scope is detailed in \ref{sec:data_feed}. The performance of the Symphony and experimental showcases are found in \ref{sec:performance}. Finally, we conclude in Section \ref{sec:conclution}.