\section{Data Feed Scope}
\label{sec:data_feed}

One of the common shortcuts taken by researchers when conducting simulation-based investigations into the performance of networking functionality in wireless sensor networks is to work at a level of abstraction that does not require the consideration of real sensory data. It is often assumed that the sensory data is instantly available for transmission and that its acquisition does not in any way interfere with the sending-receiving processes. In addition, protocols are often stress tested using synthetic cross traffic.

However, in reality, the flow of sensory data through wireless sensor nodes has significant effects on the performance of all of the network’s software components. In brief, before it can transmit the data, the sensor must warm up, sample the environment, pre-process the data, and packetize it. All of these operations take time. Moreover, if the data handling component is not implemented correctly, it may prevent the execution of the sending/receiving procedure and thereby violate the logic of the protocol being studied. Things become even more complicated when the external physical process sampled by the sensor is hard to adequately model mathematically (for packet generation purposes). In many cases, practitioners are most interested in problems of performance and correctness that occur under specific conditions in the physical world. None of the current network simulators allow the user to work with realistic sensory data traces. Symphony has a native tool for addressing this issue in the form of its Data Feed scope, which makes it possible to work with either pre-recorded real data traces or data that is fed into the Symphony node in real time from real hardware. These techniques introduce the possibility of performing experiments on the entire data pathway, examining the integrity of the data that is delivered to the backbone system, and experimenting with real time services based on data flows from WSN.


\begin{figure}[!ht]
 \centering
 \begin{center}
 \includegraphics[width=0.8\columnwidth]{sensory-data.png}
 % figure.png: 1193x594 pixel, 300dpi, 10.10x5.03 cm, bb=0 0 286 143
\end{center}

 \caption{Architecture of the Data Feed scope that supports the sensor model.}
 \label{fig:sensory-scope}
\end{figure}


The architecture of the Data Feed scope is shown in \autoref{fig:sensory-scope}. Symphony can handle both pre-recorded raw data and data supplied in real-time from an external generator or via a numerical data list. The \textit{Data Generator} interprets and retrieves data from specified locations. Its main purpose is to hide the particular implementation of data retrieval and make the sensory data available to the \textit{Sensor Model} in a generic format. Two sensor types are supported by the model: active sensors, which issue interrupts when data becomes available, and passive sensors that release data in response to periodic polling by the OS. The \textit{Sensor model} makes the data available to the operating system of the sensor node with delays that are specified in the appropriate configuration file. For active sensors, the model will issue an interrupt according to timestamps provided by the data generator. When the OS issues a call for a data reading to a passive sensor, the sensor model will look up the data in a list using the current time as a key.

Before the simulation begins, all nodes register their sensors with the \textit{SensorContainer}. The \textit{Dispatcher} block then helps in connecting the data from the \textit{Data Generator} to the appropriate \textit{Sensor model} of the node. 
  
The sensor model is configured in a similar way to the other Symphony models, which are described in the preceding sections. In addition to the calls and callbacks, the sensor model has a property element that specifies the data source as shown in line 10 of Listing \ref{lst:rawsensor}. This tells the model to read the sensory data from a user-specified file.  

\begin{minipage}{\columnwidth}
\lstset{language=XML,
caption={A representative sensory device model configuration file in XML format.},
xleftmargin=3mm,
label=lst:sensor_scope,
,escapeinside={@}{@}
}
\begin{lstlisting}
<symphony>
  ...
  <scope name=sensor>
@\label{lst:rawsensor}@  <model name="rawsensor">
    <callback return="int" params="1" param1="uint8_t" name="sensorStartDone"/>
    <callback return="int" params="1" param1="uint8_t" name="sensorStopDone"/>
    <callback return="int" params="3" param1="uint8_t" param2="uint16_t" param3="void *" time="20" units="ms" name="interruptData"/>
    <call name="SplitControlStart"/>
    <call name="SplitControlStop"/>
@\label{lst:data_source}@   	 <property name="data_source" source="/home/ubuntu/syphony/sensorydata/temperature/" type="file" />
    </model>
  </scope>
  ...
</symphony>
\end{lstlisting}
\end{minipage}
\vspace{2mm}


