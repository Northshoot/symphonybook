\section{Previous Work}
\label{sec:related}
Simulations are the preferred tool for experimentation with communication networks for technical, logistical, and cost reasons. Following the emergence of WSN technology, various general purpose network simulators (e.g. ns-2 \cite{ns2}, ns-3\cite{ns3}, Omnet\cite{omnet}, Qualnet\cite{qualnet}, etc.) have been extended by the addition of WSN- specific frameworks. However, WSN technologies have two features that make their simulation more challenging than that of typical high-end communication systems: delays introduced by the use of low-end hardware components, and the peculiarities of the execution models used in the hardware’s operating system. Extensive surveys of existing simulation tools have been presented by various authors (see \cite{5561571,survey_simtools,simsurvey2010} and references therein). In order to avoid unnecessary repetition, this article discusses only the most widely used existing tools and focuses on the problem of closing the gap between simulated and real WSN software as well as the unique features of Symphony that are listed in Section \ref{sec:intro}.

Over the last decade, numerous protocols for use in WSNs have been proposed in the literature. In most cases, their functionality was implemented and tested in artificial environments created inside general purpose network simulators. Details of these implementations are not generally available  \cite{sensor_deployment}. This situation has been criticized extensively
\cite{critique_wsn,mac_issiues,mac_deploymen}. As a result, many practitioners have been forced to implement protocols from scratch, highlighting the gap between simulator-specific implementations and implementation on real hardware platforms \cite{deploy_1,deploy_2,deploy_3,deploy_4}. The remainder of this discussion deals exclusively with simulation tools that are supplied with mainstream operating systems.


\newcolumntype{L}[1]{>{\hsize=#1\hsize\raggedright\arraybackslash}X}%
\newcolumntype{C}[1]{>{\hsize=#1\hsize\centering\arraybackslash}X}%

\setlength{\extrarowheight}{3pt}
\begin{table}[!t]
\tbl{Functionality comparison of selected 
simulators.\label{tab:sim_benchmarking}}
\centering 
\begin{tabularx}{0.98\textwidth}{ |L{8.8}|C{5.5}|C{4.1}|C{4}|C{5}|C{4}| }
\hline
\textbf{\underline{Features}} & {\textbf{Symphony}} & {\textbf{TOSSIM}} & {\textbf{Cooja}} & {\textbf{FreeRTOS}} & {\textbf{ns3}}\\
\hline
\textbf{Real code base} & Yes  & Yes\footnotemark[1] & Yes \footnotemark[2] & To some extent \footnotemark[3]  & no\\
\hline
\textbf{Execution model} & Yes & Yes\footnotemark[1] & Yes & Yes, \footnotemark[4] & -\\
\hline
\textbf{Real time} & Yes & No & Yes & No & Yes \\
\hline
\textbf{Hardware emulation} & Yes, via models & No & Limited \footnotemark[5] & No & Yes, via models\\
\hline
\textbf{Hardware induced delays} & Yes & No & To some extent & No & No\\
\hline
\textbf{Energy models} & Yes & Yes\footnotemark[6] & Yes & No &  Yes\\
\hline
\textbf{Clock skew} & Yes & No & No & No &  No\\
\hline
\textbf{Different applications} & Yes & No & No & No & Yes\\
\hline
\textbf{Different OS} & Yes & No & No & No &  -\\
\hline
\textbf{Customizable simulation detail} & Yes & No & Yes & No &  -\\
\hline
\textbf{Realistic sensor data feed} & Yes & No & No & No &  No\\

\hline
\textbf{Scalability} & Limited by hardware & 20000 & $<$ 20000\footnotemark[7] & - &  350000\\
\hline
\textbf{Up to date with OS} & Yes & Yes & Yes & 2010 & -  \\
\hline
\end{tabularx}
\end{table}

\footnotetext[1]{The real code is preserved to some extent. The node is represented as an entry in a table. }
\footnotetext[2]{Provides two modes for simulation, one based on the native code and one based on simulated code.}
\footnotetext[3]{3The code is cross-compiled so that it can be run as a \textit{posix} thread.}
\footnotetext[4]{FreeRTOS acts as a scheduler for \textit{pthreads} within a process.}
\footnotetext[5]{Only few microcontroller and radio devices are supported.}
\footnotetext[6]{PowerTOSSIM implements energy modeling. However, is very out of data not supported anymore.}
\footnotetext[7]{Less then 20000 simulated nodes, approximately 100 emulated nodes. The high number of simulated nodes comes at the cost of making the duration of the simulation greater than real-time.}


Operating systems for WSNs generally follow one of three design paradigms:  event-driven (e.g.
TinyOS\cite{citeulike:2425533}), threaded (e.g. Contiki\cite{contiki}) and
a mixture of the two (for detailed overviews of WSN operating systems, see \cite{wsnos_survey,1593549,wsn_survey_08} ). While each paradigm has its pros and cons, operating systems of all three types are available on the market and the performance of a given set of distributed algorithms and communication protocols can vary dramatically depending on the choice of the underlying OS and the composition of the software modules \cite{os_impact}. In this section, we focus on benchmarking Symphony’s functionality against the simulation facilities supplied with the three most popular operating systems for WSN: Contiki, TinyOS and FreeRTOS. Other operating systems are not considered either because their development has been abandoned or due to their proprietary code bases. The simulation tools provided with the currently used operating systems are primarily intended for simple debugging purposes. Previous attempts to increase their sophistication rapidly became outdated with the appearance of new versions of the relevant operating systems. Examples of such abandoned simulators that had similar functionality to Symphony in some respects include EmStar \cite{emstar}which provided node virtualization and was discontinued 2005; Atemu\cite{atemu}, which made it possible to perform simulations using real code (TinyOS) and was discontinued in 2004; Avrora\cite{avrora}, which provided precise timing models and was discontinued in 2009;  PowerTOSSIM\cite{powertossim}, which enabled energy modeling and was discontinued in 2010. It should be noted that none of these extensions provided all of the features of Symphony or combined them in an integrated way.


Table \ref{tab:sim_benchmarking} compares the functionality provided by existing WSN simulators to that of Symphony. When reviewing this table, one point relating to simulation tools that provide instruction-level emulation of software should be noted. Cooja is typical of such simulation environments in that it has an integrated microcontroller emulator that enables the user to perform instruction-level simulations. Symphony takes a different approach: instead of emulating a specific microcontroller, it models the behavior of diverse hardware components in terms of their execution time for specific operations and energy consumption. The time and energy parameters for individual hardware components recreated in Symphony are derived by conducting measurements on real devices while performing specific operations.


We argue that no currently available simulator offers the same range of features as Symphony or gives the user as broad a range of experimental options. One of the most important things that sets Symphony apart is its use of the popular ns-3 simulator as its core platform for the orchestration and execution of simulation experiments and as a source of well-established radio propagation models. This enables developers to experiment with holistic machine-to-machine systems that incorporate heterogeneous radio technologies, such as the communications systems of backbone networks. Secondly, Symphony uses real virtualized operating systems that are integrated into the ns-3 simulations, enabling the developer to experiment with multiple different implementations of a given distributed algorithm in a single simulation. Finally, Symphony contains a set of models that accurately mimic the execution times and energy consumption of various hardware components. These features mean that Symphony simulations can accurately reproduce the behavior of real-world WSN systems.







