\section{Introduction}
\label{sec:intro}
Simulations are the most widely used tools for analyzing the performance of protocols for communications networks. They are also used extensively to study the performance of wireless sensor networks (WSNs). However, WSNs have an important peculiarity that complicates simulation-based studies. Most (if not all) network simulators execute experiment scenarios in high-end machines but in WSNs, the limited resources of the network hardware are often a major performance-limiting factor. WSN simulations that do not account for hardware delays, time skew, delays associated with sensory data flows, and the execution model of the hardware’s operating system therefore often produce unrealistic performance figures.


This article describes a simulation framework known as Symphony that was designed for the testing and validation of WSN applications. The framework accurately reproduces the processes that occur inside real WSN equipment, including both hardware- and software-induced delays, and the dynamic flow of sensory data. Figure \ref{fig:symphony_framework} shows its high level architecture. The overall purpose of Symphony is to provide a holistic framework in which the development of WSN software and simulations of its functionality can be performed in a single integrated development environment. In brief, when using Symphony, a WSN developer always has access to a \textit{real} implementation of their application in an OS that is used in WSN hardware such as TinyOS, FreeRTOS or Contiki. Symphony uses virtualization and hardware modeling techniques that allow the developer to work on a \textit{real} node while also smoothly integrating the real implementation of the application with a general purpose network 
simulator that enables extensive testing of its distributed functionality in a controlled and repeatable manner. Technically, Symphony consists of four operating and programming scopes: a software scope, a hardware scope, a data feed scope, and an orchestration and communication scope.

%\begin{figure}[!tb]
\begin{wrapfigure}{r}{0.55\textwidth}
 \centering
\includegraphics[width=0.55\textwidth]{symphony-framework.png}
 \caption{A high level architectural overview of Symphony.}
 \label{fig:symphony_framework}
\end{wrapfigure}
%\end{figure}

 The \textit{software scope} provides the tools required to create a virtual image of an existing WSN operating system and a set of rules for doing so. The hardware scope consists of a set of models that accurately emulate the delays and energy consumption of various WSN hardware components. The \textit{data feed scope} provides tools and models for making sensory data available to the virtualized node. Finally, the \textit{orchestration and communication scope} is provided by a popular network simulator \textit{ns-3}\footnote{http://www.nsnam.org/}, which enables the straightforward creation and execution of various simulated scenarios 


Symphony thus bridges the gap between simulated and real WSN software. We anticipate that its use will significantly reduce the time required to develop practical large-scale distributed WSN systems. Symphony has numerous features that make it a unique development environment. Specifically, it:

\begin{enumerate}
 \item Enables the user to experiment with the code base that would be used in a real deployment;

 \item Preserves the execution model of the underlying operating system;

\item Accounts for the effect of hardware-induced delays on the performance of distributed applications and protocols;

\item Enables experimentation with a range of clock skew models;

\item Enables experimentation with several  different  applications  and  different  WSN  operating  systems within a single simulation;

\item Provides a customizable level of simulation detail, ranging from fine-grained firmware emulation to system-level experiments;

\item Allows the user to investigate performance-related phenomena across the entire sensory data path.

\end{enumerate}   

The article is organized as follows. Section \ref{sec:related} provides a brief review of relevant previous work. Section \ref{sec:symphony} outlines the architecture of Symphony. Section  \ref{sec:software_scope} provides more details on the software scope. The hardware scope is detailed in Section \ref{sec:hardware_scope}. The data feed scope is presented in Section \ref{sec:data_feed}. The performance of Symphony is discussed in Section \ref{sec:performance}, and Section \ref{sec:conclution} summarizes the findings and concludes the article.