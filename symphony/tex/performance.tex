\section{An Experimental Showcase and Performance Metrics for Symphony}
\label{sec:performance}

 \begin{figure}[!ht]
\centering 

\includegraphics[width=0.60\textwidth]{sec_alg_performance} 
 \caption{Number of received packets at the sink.} 
\label{tab:experiment_results}
 \end{figure}

The preceding sections have outlined the key capabilities of Symphony. Given their diversity, Symphony could potentially be used to perform benchmarking studies on a very wide range of real-world communications systems in an equally wide range of scenarios. A great deal of space would be required even to present only a selection of illustrative cases. Therefore, this section focuses on the results of a single set of experiments using different security extensions of the data packet forwarder from TOSSIM. The results of Symphony simulations are compared to those obtained using a test bed of real nodes. In addition, the run time performance of Symphony is discussed.

\subsection{Performance of the showcase scenario}

We selected a case involving a computationally demanding encryption function that is known to affect the performance of various network protocols in order to demonstrate the various unique features of Symphony. The real world consequences of using this function cannot be reproduced using traditional simulation tools.

A total of six security schemes were implemented in TinyOS. A testbed consisting of 10 devices  \cite{ril_tinymulle} was used to generate real-world results that could be compared to the simulated data. The test scenario involves a chain topology consisting of 10 nodes. The source node generates data packets of 35 bytes each. A new packet is generated when the previous one is received by the sink node. To facilitate comparisons, the total number of packets received by the sink node during the test run was counted. Each experiment was repeated ten times and an average number of received packets was calculated in each case. The same settings were used in TOSSIM, Symphony, and the testbed. The hardware scope was configured using delay and current consumption values that were determined during the execution of the security algorithms on real-world hardware while the radio transceiver was being used.

The advantages of Symphony were apparent even in the simplest experiment involving communication between the nodes with no security function (the results for this case are indicated by the label  ``Plain'' in \autoref{tab:experiment_results}). The TOSSIM results for this scenario were 10 \% more optimistic results than those obtained using the testbed. This is because TOSSIM does not account for the delays introduced by the hardware when transmitting data. More erroneous results appear when computationally intensive operations are introduced. In the experiments with the security functions, between  90 to 100 \% of the results obtained using TOSSIM were erroneous. This is in line with expectations because like all of the other current WSN simulators, TOSSIM cannot account for software-induced delays. This shortcoming means that all current simulators would give completely inaccurate estimates of network protocol performance for the test case. In contrast, the results obtained using Symphony had an average 
accuracy of 99 \% accurate. The few erroneous results were due to the inability of the ns3 channel model to fully describe the testbed environment.

\subsection{Symphony's run time performance }

The run-time performance of Symphony depends on the mode in which the framework is operating,
i.e.	Whether it is running in real- or virtual time. In the case of virtual time operation, the performance of Symphony depends on the desired granularity of the simulation, the complexity of the network topology, and the nature of the scenario. Symphony’s runtime performance in real-time mode for the showcase scenario is particularly interesting because real-time mode is one of the features that differentiate Symphony from other WSN simulation tools. This section therefore focuses on the real-time performance results; an assessment of Symphony's performance in the virtual time operating mode will be presented elsewhere.
There are two operation types that consume time during simulations and can potentially affect their accuracy: library loading and function calls in a virtualized node image. The time required to load a large number of libraries can affect the simulation bootstrapping procedure. Obviously, one needs to wait until all libraries are loaded before starting the simulation. Figure 
 \ref{fig:open_libs} shows the dynamics of this characteristic for different numbers of simulated nodes. It is readily apparent that the loading times for scenarios with fewer than 5000 nodes are practically negligible. Beyond this point, the loading time increases linearly with the number of nodes. Symphony accounts for this behavior by having a special synchronization function to ensure that simulations start up correctly.



 \begin{figure}[!t]
\centering 
  \subfloat[Time consumed by opening image.]
{\label{fig:open_libs}
\includegraphics[width=0.50\textwidth]{open-experiments} }
 %\hspace{0.02\textwidth}  
  \subfloat[Function call time in relation to simulations granularity.]
{\label{fig:func_call_mixed}
\includegraphics[width=0.5\textwidth]{execution-experiments} }
 \caption{Symphony's performance measured on Intel i7 CPU with 32 GB memory.} 
 \end{figure}



In Symphony, the granularity of the simulated model is reflected in the size of the compiled library: the lower the abstraction level, the larger the library (due to the larger code base required). Loading large numbers of large libraries into the simulator causes frequent cache misses at the CPU level of the host machine during context switching. This inevitably increases the time required to call any function in a given library. Figure \ref{fig:func_call_mixed} shows the average time per call for different library sizes and node counts within a simulation. The call time depends only on the size of the library and not on the number of nodes for a given size.

This is a hardware-imposed limitation. While it does not affect the accuracy of experiments performed in \textit{simulated} time, the user must account for this delay when constructing large scale 
\textit{real-time} scenarios. In particular, one must ensure that the processing time for an event involving an execution chain of $n$ calls is within the real-time constraints of the application. In practice, the process for interpreting the results in \ref{fig:func_call_mixed} is as follows, assuming that the library size for the chosen level of granularity is 61 KiB and that it takes 20 $\mu
s$ to execute an operation on a real hardware unit. When configuring this delay in Symphony's hardware scope, one should account for an additional delay of 6 $\mu s$ due to context switching.
In practical terms, this result should be interpreted as follows: in a scenario involving $X$ nodes where each node image is 27 KiB in size, the time required to execute an event involving $z$ calls has to be reflected in the execution time for the node model, which is defined in the hardware scope.