\section{Software Scope}
\label{sec:software_scope}
This section provides details on Symphony’s software scope. Recall that Symphony does not make any short cuts when simulating WSN functionality: a real operating system is virtualized and its execution model is preserved. In essence, Symphony intercepts calls at the desired level of abstraction and delays their execution according to the profile of the corresponding component. Symphony can be used to perform simulations on three tiers: low, medium and high. Higher tiers correspond to increased granularity in the simulation and therefore more complexity. The effects of simulation granularity on Symphony's performance are discussed in Section \ref{sec:performance}; the following subsections provide further details on each tier.  

\subsection{Application Tier}
The application tier is used to perform system-level simulations and represents the highest level of abstraction available in Symphony: only the highest level calls are passed through. This tier yields the fastest simulations. 

\begin{figure}[!ht]
\centering 
  \subfloat[Energy consumption for different security algorithms.]
{\label{fig:energy_per_operation}
\includegraphics[width=0.55\textwidth]{energy_per_operation_softaes_7part} }
  \subfloat[Encryption operation in time domain for algorithm sec\_1.]
{\label{fig:pwr_ciphers_16_ccm_enc_dec_auth}
\includegraphics[width=0.35\columnwidth]{pwr_ciphers_16_ccm_enc_dec_auth} }
 %\hspace{0.02\textwidth}  

 \caption{Measurements of energy consumption and execution time of different security
extensions. } 
\label{fig:profile_security}
 \end{figure}

Figure \ref{fig:profile_security} shows an example in which application-tier simulations are used to profile a complex security function with respect to its execution time and the system's energy consumption. Since the particular security algorithms used in the experiments are not relevant to the architecture of Symphony, they are not described in this article. For simplicity, the profiled components are referred to as \textit{sec\_X}, where \textit{X} is the ID number of a particular security function. Experiments were performed using six different security functions in total, yielding the predicted energy consumption values shown in  Figure \ref{fig:energy_per_operation}. Figure \ref{fig:pwr_ciphers_16_ccm_enc_dec_auth} shows the energy consumption of the simulated system over time for software component sec\_1, whose XML model is shown \autoref{lst:software_scope}. 

\begin{minipage}{\columnwidth}
\lstset{language=XML,
caption={Part of an XML device model that describes the execution time and energy consumption associated with the software component sec\_1},
xleftmargin=3mm,
label=lst:software_scope,
,escapeinside={@}{@}
}
\begin{lstlisting}
<symphony>
  <scope name=software>
@\label{lst:encryption}@    <model name="encryption" time_unit="milli" energy_unit="mA">
      <call time="60" e="30" name ="encrypt"/>
      <callback time="52"  e="27" name ="encrypt_done"/>
    </model>
  </scope>
  ...
</symphony>
\end{lstlisting}
\end{minipage}
\vspace{2mm}




\subsection{Operating System Tier}
Operating system components are profiled in a similar way to that discussed above. Figure \ref{fig:amsend} shows the performance of the AMSend component of TinyOS when sending two bytes of data; the corresponding XML configuration file is shown in  \autoref{lst:os_scope}. The energy consumption for this component was calculated by integrating the current flow over time.

\begin{figure}[!ht]
\centering 
  \subfloat[OS tier: Sending 2 bytes of data with AMSend.]
{\label{fig:amsend}
\includegraphics[width=0.45\textwidth]{amsend.pdf} }
  \subfloat[Driver tier: Measurements of radio device performance when transmitting.]
{\label{fig:sendRateGain}
\includegraphics[width=0.45\columnwidth]{energy_per_packet.pdf} }
 %\hspace{0.02\textwidth}  

 \caption{Profiling at the OS- and driver tiers. } 
\label{fig:radiosend}
 \end{figure}


\begin{minipage}{\columnwidth}
\lstset{language=XML,
caption={Part of an XML device model describing the execution time and energy consumption for a system-level component. },
xleftmargin=3mm,
label=lst:os_scope,
,escapeinside={@}{@}
}
\begin{lstlisting}
<symphony>
  <scope name=hardware>
@\label{lst:radio_model_soft}@    <model name="Radio" time_unit="micro" energy_unit="mA">
      <call name="AmSend" time="35" energy="1" />
      <callback return="int" params="2" param1="uint8_t" param2="void *" time="550" energy="10" name="AmSendDone"/>
      ...
    </model>
  </scope>
  ...
</symphony>
\end{lstlisting}
\end{minipage}
\vspace{2mm}



\subsection{Driver Tier}
The profiling of the node on the driver tier is graphically presented in the shaded are of  \ref{fig:os-call-hw-example}. In this case, profiling is performed at the level of the hardware abstraction layer (HAL), and the execution time and energy consumption are measured for each operation of interest.

 \begin{figure}[!ht]
\centering 
\includegraphics[width=0.40\textwidth]{os-call-hw.pdf} 
 \caption{Execution time flow in hardware/emulation.} 
\label{fig:os-call-hw-example}
 \end{figure}



Figure \ref{fig:sendRateGain} shows the calculated energy consumption for the node in the test case based on measurement conducted while transmitting packets with varying data rates and varying gains. The configuration file required to generate the desired measurements and then transmit a two-byte data packet \footnote{In this configuration example and the example in the previous section, we profiled the transmission of two bytes of data. In practice, however, profiling should be performed using data packets of the size required by the target scenario. } for this case is shown in  \autoref{lst:radio_model_hard}. 

\begin{minipage}{\columnwidth}
\lstset{language=XML,
caption={Part of an XML device model describing the execution time and energy consumption for  a hardware component. },
xleftmargin=3mm,
label=lst:radio_model_hard,
,escapeinside={@}{@}
}
\begin{lstlisting}
<symphony>
  <scope name=hardware>
@\label{lst:radio_model}@    <model name="Radio" time_unit="milli" energy_unit="mAs">
      <call name="RadioSend"/>
@\label{lst:cca_done}@      <callback return="int" params="1" param1="uint8_t" time="19.2" energy="0.1" name ="RadioSendDone"/>
      ...
    </model>
  </scope>
  ...
</symphony>
\end{lstlisting}
\end{minipage}
\vspace{2mm}