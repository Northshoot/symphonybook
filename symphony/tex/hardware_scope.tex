\section{Hardware Scope}
\label{sec:hardware_scope}

Hardware interrupts and calls are emulated by tapping into the hardware abstraction layer (HAL) of the WSN OS. As shown in Figure \ref{fig:os-call-hw-example}, when an operating system makes a call to a hardware element (in this case, a call to a radio transceiver to transmit a message) in Symphony, the call is dispatched to the appropriate hardware model. Essentially, the device model is a piece of software that mimics the behavior of the real hardware component. Technically, all of the modes used in Symphony are inherited from the \textit{ns3::Object} class and parameterized according to the appropriate XML configuration file.

For example, a model of a temperature sensor will read temperature data (as discussed in Section \ref{sec:data_feed} below) and delay the callback by the amount of time that the real device would take to perform the same operation, which is specified in the XML profile. The model of the RF230 transceiver used in the above examples can be linked to any one of the ns3 wireless channel models.

The remainder of this section describes the implementation of a 'crystal device' in Symphony and its modes of operation. This component is essential for performing real-time studies of distributed embedded systems.



\subsection{The Clock Model - Simulating Time Skew}
A typical WSN node is equipped with one or two crystals that power the device's internal clocks. These crystals generate ticks at a certain frequency and the software counter transforms these ticks into time measurements by rounding them to a specific value. For example, TinyOS \footnote{http://www.tinyos.net/tinyos-2.x/doc/html/tep102.html} defines one second in milliseconds as 1024 ticks. These clocks have a degree of drift due to differences in the oscillation frequencies of crystals from different batches. The curve in Figure \ref{fig:sync-desturbance} shows the clock drift measured on a real device. Most current network simulators lack native means of accounting for clock drift and just use simulated clocks that have no deviation (represented by the red line in figure 7a) for all nodes. However, time skew is widely recognized to be a significant problem in WSN and has been studied extensively  \cite{clock_sync_3,timeSyncSurvey4}. Most academics who conduct experimental work on network functionality assume that perfect time 
synchronization can be achieved by \textit{specialized algorithms or hardware} \cite{missionCriticalMac}. 


\begin{figure}[!ht]
\centering
  \subfloat[Consequences of clock drift.]
  {\label{fig:sync-desturbance}
  \includegraphics[width=0.5\textwidth]{hmac_ftsp_30min.pdf} }   
  %\hspace{0.02\textwidth} 
  \subfloat[Histogram of clock ticks affected by random skew.]
  {\label{fig:skew_time_random}
  \includegraphics[width=0.42\textwidth]{skew_histogram.pdf} } 
  \caption{Consequences of time skew (a) and a histogram showing the output of a random skew model (b).} 
\end{figure}



 \begin{figure}[!ht]
\centering 
  \subfloat[Static time skew with period of 1000 $\mu$s.]
{\label{fig:skew_tos_static_1000}
\includegraphics[width=0.50\textwidth]{skew_time_tos_static_1000} }
 %\hspace{0.02\textwidth}  
  \subfloat[Exponential time skew.]
{\label{fig:skew_time_tos_exponential_5000}
\includegraphics[width=0.5\textwidth]{skew_time_tos_exponential_5000} }

 \caption{Crystal simulations.} 
\label{fig:time_skew_simualtions}
 \end{figure}

Symphony features a native real-time clock model called \textit{SimuClock}. When connected to an emulated node, this model generates \textit{ticks} according to a specification provided by the user in an XML configuration file. By default, no clock drift is applied. However, the user can configure the clocks to drift linearly, exponentially or randomly. The random clock drift is implemented using  \textit{Random Variable Distributions} of ns3. A histogram showing the number of ticks per second generated using the normal distribution is shown in \ref{fig:skew_time_random}. The linear clock drift is implemented by \textit{drifting} the clocks by a constant quantum of time as shown in Figure \autoref{fig:skew_tos_static_1000}. If an exponential drift is chosen, the drift quantum doubles periodically as shown \autoref{fig:skew_time_tos_exponential_5000}.  



The SimuClock model can be configured in two ways: either by altering the node model description using XML as shown in  Listing \autoref{lst:clock_drift} or by modifying the ns3 simulation file as shown in  Listing \autoref{lst:clock_configuration}. In both cases, the XML description follows the previously used convention: the model is defined and then the desired properties of the model are declared.


\begin{minipage}{\columnwidth}
\lstset{language=XML,
caption={Part of an XML configuration file showing the parameterization of SimuClock.},
xleftmargin=3mm,
label=lst:clock_drift,
,escapeinside={@}{@}
}
\begin{lstlisting}
<symphony>
  <scope name=hardware>
  ...
@\label{lst:simu_clock}@    <model name="SimuClock">
  <property name="config" type="random" drift="1ms" randommean="8" radomvariance="4" driftperiod="5ms" />
    </model>
  ...
  </scope>
  ...
</symphony>
\end{lstlisting}
\end{minipage}
\vspace{2mm}

Since Symphony uses ns-3 to orchestrate and execute simulations, all of its models could potentially be configured using the native ns3 configuration tools. This is illustrated in  \autoref{lst:clock_configuration}, which shows how one could configure the clock model via  \texttt{Config::SetDefault}. Clock drift is disabled by default (\texttt{SimuClock::NONE}) and  so no further configuration is required if clock drift is not desired. If drift is desired, various attributes will have to be configured as shown in the listing, depending on the nature of the drift that is required.

\begin{minipage}{\columnwidth}
\lstset{language=C++,caption={Configuration of the clock model in the ns-3 environment.
},xleftmargin=3mm,
xrightmargin=3mm,
label=lst:clock_configuration}
\begin{lstlisting}
#include <stdio.h>
...//Standard ns-3 modules are omitted.
#include "ns3/symphony-module.h"
using namespace ns3;
int main(int argc, char *argv[]) {
  ...
  Config::SetDefault ("ns3::SimuClock::ClockDriftType", EnumValue(SimuClock::RANDOM));
  Config::SetDefault ("ns3::SimuClock::ClockDrift", TimeValue(MicroSeconds(1)));
  Config::SetDefault ("ns3::SimuClock::RandomMean", DoubleValue(8));
  Config::SetDefault ("ns3::SimuClock::RandomVariance", DoubleValue(4));
  Config::SetDefault ("ns3::SimuClock::ClockDriftPeriod", TimeValue(MilliSeconds(5)));
  ...
  return 0;
}
\end{lstlisting}
\end{minipage}

% \begin{figure}[!ht]
% \centering
%  \subfloat[Histogram of clock ticks affected by random skew.]
% {\label{fig:histogram_skew_random}
% \includegraphics[width=0.5\textwidth]{skew_histogram} }   
% %\hspace{0.02\textwidth}
%  \subfloat[Time skew using random model.]
% {\label{fig:skew_time_random}
% \includegraphics[width=0.5\textwidth]{skew_time_tos_random} }
%  \caption{Random time skew.}
%  
 
% \end{figure}